import os
import subprocess
import time
from loguru import logger


fmt = "{time} - {name} - {level} - {message}"
logger.add("logging/spam.log", level="DEBUG", format=fmt, backtrace=True, diagnose=True)
logger.add("logging/error.log", level="ERROR", format=fmt, backtrace=True, diagnose=True)
logger.add("logging/info.log", level="INFO", format=fmt, backtrace=True, diagnose=True)

repo_dict = {
    "bkmks": "/home/mic/python/bkmk",
    "bkmks_urwid": "/home/mic/python/bkmks_urwid",
    "books": "/home/mic/python/books",
    "cli_app_list": "/home/mic/python/cli_app_list",
    "micro_diary": "/home/mic/python/micro_diary",
    "notes": "/home/mic/python/notes",
    "old_alternative_projects": "/home/mic/python/old_alternative_projects",
    "player": "/home/mic/python/player",
    "pwd": "/home/mic/python/pwd",
    "rss": "/home/mic/python/rss",
    "scraper": "/home/mic/python/scraper",
    "scripts": "/home/mic/scripts",
    "todos": "/home/mic/python/todos",
    "tree": "/home/mic/python/tree",
    "url": "/home/mic/python/urlshort",
    "hexo-python-blog": "/home/mic/hexo-python-blog",
    "hexo_poems": "/home/mic/hexo-poems",
    "http": "/srv/http",
}


@logger.catch
def repositories():
    """Verify what repos need an update"""
    repositories.lst = []
    path = []
    repositories.chaves = []

    logger.info(repo_dict)
    for value in repo_dict.values():
        path.append(value + "/status_results.txt")
    logger.info(path)

    for i in path:
        if "Changes not staged for commit" or "Untracked files" in i:
            repositories.lst.append(i)
    logger.info(repositories.lst)
    for i in repositories.lst:
        repositories.chaves.append(i.rsplit("/")[-2])
    logger.info(repositories.chaves)


if __name__ == "__main__":
    repositories()


@logger.catch
def update():
    "Here we do the update"
    for i in repositories.chaves:
        cmd = "/home/mic/scripts/git_manager/" + i + ".exp"
        subprocess.run(cmd, shell=True)

    logger.info(cmd)


if __name__ == "__main__":
    update()
