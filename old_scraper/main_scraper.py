#!/usr/bin/python3.10
"""Main module of the app. Where all functionalities are accessed from"""

import scrapy
from scraper import Scraper
from loguru import logger

fmt = "{time} - {name} - {level} - {message}"
logger.add("spam.log", level="DEBUG", format=fmt)
logger.add("error.log", level="ERROR", format=fmt)


def main():
    """Here we start, sequentially, all the steps needed to create a scraping campaign."""

    rasoura = Scraper(
        "quanta",
        "quanta_magazine",
        "/home/mic/python/scraper",
        "https://www.quantamagazine.org/first-time-crystal-built-using-googles-quantum-computer-20210730/",
        '//*[@id="postBody"]/div[1]/section/section/div/div[2]/div/h1/text()',
        '//*[@id="postBody"]/div[2]/div/div/div/aside[1]/div/div[1]/div[1]/div/a/h3/text()',
        '//*[@id="postBody"]/div[2]/div/div/div/aside[1]/div/div[1]/div[1]/p/em/text()',
        '//*[@id="postBody"]/div[2]/div/div/div/section/div/div/div/p/text()',
    )

    rasoura.dislocation1()
    rasoura.start_project()
    rasoura.start_spider()
    rasoura.edit_spider_file_clean_file()
    rasoura.edit_spider_file_parse_function()
    rasoura.settings()
    rasoura.crawl()
    rasoura.result()


main()
