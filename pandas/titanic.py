"""Module Docstring"""
from loguru import logger
import pandas as pd

fmt = "{time} - {name} - {level} - {message}"
logger.add("info.log", level="INFO", format=fmt, backtrace=True, diagnose=True)
logger.add("error.log", level="ERROR", format=fmt, backtrace=True, diagnose=True)


@logger.catch
def titanic():
    """"""
    titanic = pd.read_csv("titanic.csv")
    print(titanic.head(15))
    print(titanic.dtypes)
    print(titanic.info())
    age_sex = titanic[["Age", "Sex"]]
    print(age_sex.head())
    over35 = titanic[titanic["Age"] > 35]
    print(over35.head(20))
    print(over35.shape)
    class2_3 = titanic[titanic["Pclass"].isin([2, 3])]
    print(class2_3.head(20))


if __name__ == "__main__":
    titanic()
