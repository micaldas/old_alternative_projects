"""Module Docstring"""
from loguru import logger
from scipy import misc
import matplotlib.pyplot as plt

fmt = "{time} - {name} - {level} - {message}"
logger.add("info.log", level="INFO", format=fmt, backtrace=True, diagnose=True)
logger.add("error.log", level="ERROR", format=fmt, backtrace=True, diagnose=True)


@logger.catch
def tut():
    """"""
    img = misc.face()
    print(type(img))

    plt.imshow(img)

    print(img.shape)
    print(img.ndim)


if __name__ == "__main__":
    tut()
