"""Here we build the document that will serve to construct the homepage."""
import subprocess

import isort  # noqa: F401
import snoop
from loguru import logger

fmt = "{time} - {name} - {level} - {message}"
logger.add("../logs/info.log", level="INFO", format=fmt, backtrace=True, diagnose=True)  # noqa: E501
logger.add("../logs/error.log", level="ERROR", format=fmt, backtrace=True, diagnose=True)  # noqa: E501

subprocess.run(["isort", __file__])


@logger.catch
@snoop
def hp_document():
    """We iterate through a semi-treated list of publications and
    their urls, to clean the output until we have simple strings,
    create a tuple with the information and keep it in a list that
    will house all sites and links."""

    with open("spls.txt", "r") as f:
        clean_lsts = f.readlines()

    with open("final_materials_list.txt", "a") as f:
        f.write("materials = [\n")

    for i in range(len(clean_lsts)):
        tit_link = clean_lsts[i]
        print(tit_link)
        publication = tit_link.split(",", 1)[0]
        print(publication)
        try:
            url = tit_link.split(",")[1]
        except IndexError:
            break
        print(url)
        end = tit_link.split(",")[2]
        print(end)
        pub = publication[1:]
        print(pub)
        link = url[3:]
        print(link)
        tup = (pub, link)
        print(tup)
        with open("final_materials_list.txt", "a") as f:
            f.write(f"{tup},\n")

    with open("final_materials_list.txt", "a") as f:
        f.write("]")

    with open("final_materials_list.txt", "r") as f:
        materials_list = f.readlines()

    print(materials_list)


if __name__ == "__main__":
    hp_document()
